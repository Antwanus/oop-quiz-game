from data import question_data
from question_model import Question
from quiz_brain import QuizBrain

question_list = []
for q in question_data:
    key = q.get("text")
    value = q.get("answer")
    question_list.append(Question(key, value))

qb = QuizBrain(question_list)

while qb.still_has_questions():
    qb.next_question()

print("-------------------------------------------------------------------\nYou've completed the quiz")
print(f"The final score is {qb.get_score()} / {qb.get_question_number()}")
print(f"The final score is {qb.score} / {qb.question_number}")
