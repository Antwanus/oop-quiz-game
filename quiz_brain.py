from question_model import Question


def check_answer(question: Question, answer: str):
    if answer.lower() == 'true' and question.answer.lower() == 'true':
        return True
    elif answer.lower() == 'false' and question.answer.lower() == 'false':
        return True
    else:
        return False


class QuizBrain:
    def __init__(self, question_list):
        self.question_number: int = 0
        self.score: int = 0
        self.question_list: list = question_list

    def next_question(self):
        """retrieves the question \n
        displays question & asks for user's answer via input()\n
        calls check_answer() & keeps score"""
        q: Question = self.question_list[self.question_number]
        self.question_number += 1
        answer = input(f"Q.{self.question_number}: {q.text} [True/False]?: ")
        if check_answer(q, answer):
            print("You got it")
            self.score += 1
        else:
            print("nope..")
        print(f"Your current score is: {self.score}/{self.question_number}")
        print("The correct answer was ", q.answer, "\n")

    def still_has_questions(self):
        return len(self.question_list) > self.question_number

    def get_score(self):
        return self.score

    def get_question_number(self):
        return self.question_number


